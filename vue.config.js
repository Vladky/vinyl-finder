module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/' + process.env.CI_PROJECT_NAME + '/' : '/',
  chainWebpack: config => {
    ;['vue-modules', 'vue', 'normal-modules', 'normal'].forEach(match => {
      config.plugin('html').tap(args => {
        args[0].title = process.env.VUE_APP_TITLE
        return args
      })
    })
  },
  pwa: {
    name: 'Vinyl Finder',
    themeColor: '#4e89ae'
  }
}
